import React from 'react'
import PropTypes from 'prop-types'

const Input = ({ type, onChange, name, value, onKeyDown, className }) => {
    return (
        <input
            className={className}
            type={type} 
            name={name}
            value={value}
            onChange={(e) => onChange(e)}
            onKeyDown={(e) => onKeyDown(e)}
        />
    )
}

Input.defaultProps = {
    type: "text"
}

Input.propTypes = {
    type: PropTypes.oneOf(['text', 'email', 'number', 'date', 'color', 'search'])
}

export default Input