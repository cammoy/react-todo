import React from 'react'
import PropTypes from 'prop-types'

const Item = ({ id, todo, completed, toggle, remove }) => {

    const handleCompleted = (completed) => completed ? '--completed' : '--incomplete'

    if( todo.length ) {
        return (
            <div className={`columns is-mobile is-multiline is-vcentered todos__item ${handleCompleted(completed)}`}>
                <div className="column is-2 has-text-centered">
                    <span onClick={ () => toggle(id)} className={`todos__item__toggle `}></span>
                </div>
                <div className="column todos__item__text is-8">{todo}</div>
                <div className="column has-text-centered is-2">
                    <span onClick={ () => remove(id)} className="todos__item__remove">&times;</span>
                </div>
            </div>
        )
    }

    return null
}

Item.defaultProps = {
    todo: {}
}

Item.propTypes = {
    todo: PropTypes.string.isRequired,
    add: PropTypes.func,
    toggle: PropTypes.func,
    remove: PropTypes.func
}

export default Item

