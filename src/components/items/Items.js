import React from 'react'
import Item from './Item'
import PropTypes from 'prop-types'
import './style.scss'

const Items = ({ items, remove, toggle }) => {
    if( items.length ) return (
        <div className="has-text-left todos">
            {items.map( (item, i) => 
                <Item {...item} key={item.id} remove={remove} toggle={toggle} />
            )}
        </div>
    )
    return null
}

Items.defaultProps = {
    items: []
}

Items.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    toggle: PropTypes.func,
    remove: PropTypes.func,
}

export default Items