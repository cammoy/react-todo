import {
    init
  } from '@rematch/core';
  import createLoadingPlugin from '@rematch/loading';
  import updatedPlugin from '@rematch/updated';
  import immerPlugin from '@rematch/immer'
  import * as models from './models';

  const loading = createLoadingPlugin({});
  const updated = updatedPlugin();
  const immer = immerPlugin()
  
  const store = init({
    models,
    plugins: [loading, updated, immer]
  });
  
  export default store;