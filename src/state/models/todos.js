import uuidv1 from 'uuid/v1';
const local = window.localStorage
let data;

if( local.getItem('todos') !== undefined ) {
  data = JSON.parse(local.getItem('todos'))
}

export default {
  state: data || [],
  reducers: {
    remove(state, payload) {
      const items = state.filter( ({ id }) => id !== payload )
      local.setItem('todos', JSON.stringify(items))
      return items
    }, 
    add(state, payload) {
      if( payload.length ) {
        let cur = [...state, { id: uuidv1(), todo: payload, completed: false }]
        local.setItem('todos', JSON.stringify(cur))
        return cur
      }
    },
    toggle(state, payload) {
      state.forEach( item => {
        if( item.id === payload ) return item.completed = !item.completed
      })
      local.setItem('todos', JSON.stringify(state))
    },
    reset(state, payload) {
      return state = payload
    }
  },
};
