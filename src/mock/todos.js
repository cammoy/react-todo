export default [
  {
    id: 0,
    todo: "Allow Add, Remove and Toggle",
    completed: true
  },
  {
    id: 1,
    todo: "Calculate total and completed",
    completed: true
  },
  {
    id: 2,
    todo: "Use function components and hooks",
    completed: true
  },
  {
    id: 3,
    todo: "Allow reset if no items and page refreshed",
    completed: true
  },
  {
    id: 4,
    todo: "Styled with SCSS",
    completed: true
  },
  {
    id: 5,
    todo: "Use Redux via rematch",
    completed: true
  },
  {
    id: 6,
    todo: "Use localstorage to perist",
    completed: true
  },
  {
    id: 7,
    todo: "Could be improved with test coverage and broader browser support",
    completed: true
  },
]