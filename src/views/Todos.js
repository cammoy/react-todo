import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Items from '../components/items'
import Input from '../components/input/'
import mock from '../mock/todos'
import './style.scss'

const local = window.localStorage
const stored = JSON.parse(local.getItem('todos'))

const Todos = ({ todos, toggle, add, remove, reset }) => {
    const [ value, setValue ] = useState('')

    useEffect( () => {
        if( stored === null || !stored.length  ) {
            reset(mock)
        }
    }, [reset])

    const hangeChange = e => {
        setValue(e.target.value)
    }

    const handleKeyDown = e => {
        if (e.key === "Enter") {
            add(value)
        }
    }

    const calculateTotals = (todos) => (
        <div className="field is-grouped is-grouped-multiline has-text-right">
            <div className="control">
                <div className="tags has-addons">
                    <span className="tag is-dark">Total</span>
                    <span className="tag is-primary">{todos.length}</span>
                </div>
            </div>
            <div className="control">
                <div className="tags has-addons">
                    <span className="tag is-dark">Completed</span>
                    <span className="tag is-info">{todos.filter( item => item.completed).length}</span>
                </div>
            </div>
        </div>
    )


    const renderTodos = (todos) => {
        if(todos.length) return (
            <Items 
                items={todos} 
                add={add}
                toggle={toggle}
                remove={remove}
            />
        )
        return (
            <div className="has-text-centered">
                <span className="button is-rounded is-warning" onClick={() => reset(mock)}>Reset to default</span>
            </div>
        )
    }

    return (
        <>
            <div className="columns is-multiline is-vcentered is-centered">
                <div className="column is-8">
                    <div className="columns is-mobile is-multiline">
                        <div className="todos__title">
                            <h1 className="title has-text-centered">Todos</h1>
                        </div>
                        <div className="column todos__total">
                            {calculateTotals(todos)}
                        </div> 
                    </div>
                    {renderTodos(todos)}
            </div>
                <div className="column is-8 todos__create">
                    <div className="columns is-mobile is-vcentered is-centered">
                        <div className="column">
                            <Input 
                                value={value} 
                                onChange={hangeChange} 
                                onKeyDown={(e) => handleKeyDown(e)}
                                className="input is-rounded is-info"
                            />
                        </div>
                        <div className="column is-4">
                            <span className="button btn-small is-rounded is-primary box" onClick={() => add(value)}>ADD</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

Todos.defaultProps = {
    items: []
}

Todos.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
}

const mapState = state => ({
    todos: state.todos
})

const mapDisptach = dispatch => ({
    add: item => dispatch.todos.add(item),
    toggle: id => dispatch.todos.toggle(id),
    remove: id => dispatch.todos.remove(id),
    reset: todos => dispatch.todos.reset(todos)
})

export default connect(mapState, mapDisptach)(Todos)