import React from 'react';
import Todo from './views/Todos'

const App = () => {
  return (
    <div className="container app">
        <Todo />
    </div>
  );
}

export default App;
